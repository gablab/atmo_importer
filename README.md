# atmo_importer

Take care of creating an `.env` file like
```
API_KEY=...
DEVICE_MAC=...

DATABASE_URI=...
```

<!-- Run with 
```
podman build -t 'atmo_importer:latest' .
podman run -it --rm --name atmo_importer atmo_importer:latest --start_date=YYYY-MM-DD --end_date=YYYY-MM-DD
``` -->

Deploy
```
SSH_HOST=...  # the hostname where yo deploy
ATMO_DIR=...  # the directory where to deploy
N_DAYS_BEFORE=...  # the number of days ago of the imported data
ssh ${SSH_HOST} bash -s -- ${ATMO_DIR} ${N_DAYS_BEFORE} < deploy_script.sh 
scp .env ${SSH_HOST}:${ATMO_DIR}
```


# Cron knowledge
see logs
```
sudo tail -f /var/log/syslog | grep CRON
```

get mails
```
sudo apt-get install postfix  # to enable mail
sudo tail -f /var/mail/gab  # to read mail (e.g. from cron)
```
