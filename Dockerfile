FROM python:3

WORKDIR /app

COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt

COPY run_acquisition.py /app
COPY .env /app

ENTRYPOINT [ "python", "/app/run_acquisition.py" ]

