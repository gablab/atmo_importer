from dotenv import dotenv_values

import requests
import pandas as pd

import click
from sqlalchemy import create_engine
import datetime


@click.command()
@click.option("--start_date", default="yesterday", help="Start date.")
@click.option("--end_date", default="yesterday", help="End date.")
def cli(start_date, end_date):

    config = dotenv_values(".env")
    API_KEY = config["API_KEY"]
    DEVICE_MAC = config["DEVICE_MAC"]
    DATABASE_URI = config["DATABASE_URI"]

    if start_date == "yesterday":
        start_date = (datetime.date.today() - datetime.timedelta(days=1)).strftime(
            "%Y-%m-%d"
        )
    if end_date == "yesterday":
        end_date = (datetime.date.today() - datetime.timedelta(days=1)).strftime(
            "%Y-%m-%d"
        )

    r = requests.get(
        "https://api.atmotube.com/api/v1/data",
        params={
            "api_key": API_KEY,
            "mac": DEVICE_MAC,
            "offset": "0",
            "limit": 24 * 60,
            "start_date": start_date,
            "end_date": end_date,
        },
    )
    j = r.json()["data"]["items"]
    df_measurements = pd.DataFrame(j)
    if df_measurements.empty:
        print("No data found")
        return

    try:
        df_measurements = df_measurements.assign(
            time=lambda x: pd.to_datetime(x.time), device_mac=DEVICE_MAC
        ).rename(columns={"t": "temperature", "h": "humidity", "p": "pressure"})
    except BaseException as e:
        print("Error in the data")
        print(df_measurements)
        raise e

    if "coords" in df_measurements.columns:
        df_measurements = df_measurements.assign(
            lat=lambda x: x.coords.apply(pd.Series).lat,
            lon=lambda x: x.coords.apply(pd.Series).lon,
        )
    else:
        df_measurements = df_measurements.assign(
            lat=None,
            lon=None,
        )

    engine = create_engine(DATABASE_URI, echo=False)

    print(f"Found {len(df_measurements)} rows")
    with engine.connect() as conn:
        df_measurements.drop(columns=["coords"], errors="ignore").to_sql(
            "atmo_measurement", conn, if_exists="append", index=False
        )
    print("Data saved to database")


if __name__ == "__main__":
    cli()
