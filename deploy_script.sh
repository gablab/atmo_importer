#!/bin/bash

set -e

ATMO_DIR=$1
DAYS_BEFORE=$2

echo setting the script in $ATMO_DIR
echo setting the acquisition for $DAYS_BEFORE days before

if [ -d "$ATMO_DIR" ]; then
    echo found directory $ATMO_DIR
else
    mkdir -p "$ATMO_DIR"
    git clone git@gitlab.com:gablab/atmo_importer.git $ATMO_DIR
fi

pushd $ATMO_DIR

git checkout main
git pull

python3 -m venv .venv
.venv/bin/python3 -m pip install -r requirements.txt

popd


echo Adding the following to your crontab:
CRON_JOB="0 6 * * * ${ATMO_DIR}/.venv/bin/python3 ${ATMO_DIR}/run_acquisition.py --start_date=\$(date -d '-2 day' '+\\%Y-\\%m-\\%d') --end_date=\$(date -d '-2 day' '+\\%Y-\\%m-\\%d') > ${ATMO_DIR}/cron.log 2>&1"
echo $CRON_JOB


# Temporary file to hold the current crontab
TEMP_CRONTAB=$(mktemp)

# Export the current crontab to the temporary file
crontab -l > "$TEMP_CRONTAB" 2>/dev/null

# Check if the cron job already exists
if grep -Fq "$CRON_JOB" "$TEMP_CRONTAB"; then
    echo "This cron job already exists in the crontab."
    rm "$TEMP_CRONTAB"
    exit 0
fi

# Append the new cron job to the temporary file
echo "$CRON_JOB" >> "$TEMP_CRONTAB"

# Install the new crontab
if crontab "$TEMP_CRONTAB"; then
    echo "Cron job added successfully."
else
    echo "Failed to add cron job."
    rm "$TEMP_CRONTAB"
    exit 1
fi

# Clean up
rm "$TEMP_CRONTAB"
